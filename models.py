# -*- coding: utf-8 -*-

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey


Base = declarative_base()

class Page(Base):
    __tablename__ = 'pages'

    id = Column(Integer, primary_key=True)
    URL = Column(String)
    request_depth = Column(Integer)

class Link(Base):
    __tablename__ = 'links'

    from_page_id = Column(Integer, ForeignKey('pages.id'), primary_key=True)
    link_id = Column(Integer, ForeignKey('pages.id'), primary_key=True)

