# -*- coding: utf-8 -*-

import re
import asyncio
from urllib.parse import urlparse, unquote
from bs4 import BeautifulSoup

from aiohttp import ClientSession
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker

from models import Page, Link
import sqlite3


class WikiParser:
    def __init__(self):
        self._loop = asyncio.get_event_loop()
        self._engine = create_engine('sqlite:///wiki.db')
        self._metadata = MetaData()

        Session = sessionmaker(bind=self._engine)
        self._session = Session()

    def _get_links(self, html_code, request_depth, from_page_id=0):
        """Парсит ссылки из html кода

           :param str html_code: код html
           :param int request_depth: глубина ссылок
           :param int from_page_id: id родительской страницы
           :return: список ссылок
           :rtype: str[]
        """

        pattern = r'<a href="(.+?)">'
        html_code = html_code.decode('utf-8')

        soup = BeautifulSoup(html_code, 'html.parser')
        body_block = soup.find('div', class_='mw-parser-output')
        
        links = []        
        if body_block is None:
            return links    
        insert_list = []
                
        # только ссылки на другие статьи (могут быть относительные и абсолютные)
        for a in body_block.find_all('a', attrs={'href': re.compile("(^/wiki|wikipedia.org)")}):
            href = unquote(a['href'])
            href = href if re.search("^http", href) else f'{self._site}{href}'
            if href not in links:
                links.append(href)
                insert_list.append({'URL': href, 'request_depth': request_depth})

                
                # todo: вставка в таблицу работает долго (порядка 0.2с для 1 страницы)
                # page_id = self._insert_into_page(href, request_depth)
                # self._insert_into_link(from_page_id, page_id)            
        self._insert_pages(insert_list)
        self._insert_links(from_page_id,  links,  request_depth)

        return links

    async def _send_request(self, url, request_depth=1):
        """Отправлят асинхронный запрос на страницы

           :param str url: url страницы
           :param int request_depth: глубина ссылки
        """
        self._log(f'send request {url}',  request_depth)
        from_page_id = self._get_page(url, request_depth).id

        # асинхронно получаем ответ
        async with ClientSession() as session:
            
            # асинхронно читаем тело ответа
            async with session.get(url) as response:

                html_code = await response.read()
                self._log(f'readed request {url}',  request_depth)
                
                links = self._get_links(html_code, request_depth+1, from_page_id)
                self._log(f'{len(links)} links',  request_depth)
        
        if request_depth < self._max_request_depth and len(links):
            await self._send_async_events(links, request_depth+1)
            
    def _log(self, text, level=0):        
        """Логирование

           :param str text: текст
           :param int level: уровень логирования
        """

        if self._logging:
            print ('  ' * level + text)

    def _send_async_events(self, url_list, request_depth=1):
        """Создаёт задачи и запускает их асинхронно
           :param str[] url_list: список url страницы
           :param int request_depth: глубина ссылки
        """

        self._log(f'Уровень: {request_depth}',  request_depth)    
        
        tasks = []

        for i,url in enumerate(url_list):            
            task = asyncio.ensure_future(self._send_request(url, request_depth))
            tasks.append(task)

        return asyncio.wait(tasks)
        
    def _get_page(self,  url,  request_depth):
        """Возвращает строку страницы из таблицы
  
           :param str url: url страницы
           :param int request_depth: глубина страницы
           :return: строку страницы
           :rtype: Query object or None
        """

        return self._session.query(Page).filter_by(URL = url, request_depth = request_depth).first()

    def _insert_into_page(self, url, request_depth):
        """Добавить строку в таблицу страниц
          
           :param str url: url страницы
           :param int request_depth: глубина страницы
           :return: id страницы
           :rtype: int
        """

        page = self._get_page(url, request_depth)
        if page is None:
            page = Page(URL=url, request_depth=request_depth)
            self._session.add(page)
            self._session.commit()
       
        return page.id

    def _insert_into_link(self, from_page_id, link_id):
        """Добавить строку в таблицу связей
          
           :param int from_page_id: id родительской страницы
           :param int link_id: id дочерней страницы
        """

        query = self._session.query(Link).filter_by(from_page_id = from_page_id, link_id = link_id)
        if not query.count():
            link = Link(from_page_id = from_page_id, link_id = link_id)
            self._session.add(link)
            self._session.commit()
            
    def _insert_pages(self,  insert_list):
        self._engine.execute(
            Page.__table__.insert(),
            insert_list
        )
        
    def _insert_links(self,  from_page_id,  links,  request_depth):
        insert_list = []
        for link in links:
            page_id = self._get_page(link,  request_depth).id
            insert_list.append({'from_page_id': from_page_id,  'link_id': page_id})
        
        self._engine.execute(
            Link.__table__.insert(),
            insert_list
        )
        
    def _setup_site(self,  base_url):
        """Определяем адрес сайта
          
           :param str base_url: url базовой страницы
        """
        
        parsed_uri = urlparse(base_url)
        self._site = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)

    def run(self, base_url, max_request_depth=1, clear_tables=False, logging=False):
        """Запускает парсер
          
           :param str base_url: url базовой страницы
           :param int max_request_depth: глубина запросов
           :param bool clear_tables: очистить таблицы перед парсингом (для разработки)
           :param bool logging: логировать (print) (для разработки)
        """

        if clear_tables:
            self._engine.execute('DELETE FROM pages')
            self._engine.execute('DELETE FROM links')
        
        self._logging = logging
        
        self._insert_into_page(base_url, 1)                
        self._setup_site(base_url)

        self._max_request_depth = max_request_depth        
        wait = self._send_async_events([base_url])
        self._loop.run_until_complete(wait)


